/*
2.2. Даны три числа, например, 0.1, 0.15 и 0.25. Числа даны в виде строки. Необходимо ответить,
является ли третье число суммой двух первых.
* Учесть локаль пользователя и разделитель целой-дробной частей в данных строках
*/

public class SumStringNumbers {
    public static void main(String[] args) {

        String a = "0,1", b = "0.15", c = "0.25";
        double strA = Double.parseDouble(a.replace(',', '.'));
        double strB = Double.parseDouble(b.replace(',', '.'));
        double strC = Double.parseDouble(c.replace(',', '.'));

        if ((strA + strB) == strC)
            System.out.println(true);
        else
            System.out.println(false);
    }
}
