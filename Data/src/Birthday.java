import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;

/*
1.1.  Определить ваш возраст с момента рождения на момент запуска программы.
Возраст в секундах, минутах, часах, днях, месяцах и годах
*/

public class Birthday {

    public static void main(String[] args) {
        LocalTime nowTime = LocalTime.now();
        LocalDate nowDate = LocalDate.now();

        Period period = Period.of(1988, 1, 7);
        LocalDate minus = nowDate.minus(period);
        String emd = minus.format(DateTimeFormatter.ofPattern("dd дней, MM месяцев, yy лет"));

        LocalTime myTime = LocalTime.of(13, 0, 0);
        Duration duration = Duration.between(nowTime, myTime);
        LocalTime minus1 = nowTime.minus(duration);
        String hms = minus1.format(DateTimeFormatter.ofPattern("ss секунд, mm минут, hh часов"));

        System.out.print("С моего рождения прошло: " + hms + " " + emd);
    }
}
