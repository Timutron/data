import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/*
1.3. Дана строка вида "Friday, Aug 10, 2016 12:10:56 PM". Необходимо сконвертировать ее в вид “2018-08-10".
(* “2016-08-16T10:15:30+08:00” в дату со временем с учетом часового пояса  - привести к ижевскому).
*/

public class StringDate {
    public static void main(String[] args) throws ParseException {
        String stringDate = "Friday, Aug 10, 2016 12:10:56 PM";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, MMM dd, yyyy HH:mm:ss", Locale.ENGLISH);
        Date date = simpleDateFormat.parse(stringDate);
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("Russian/Samara"));
        String newDate = simpleDateFormat.format(date);
        System.out.println(newDate);
    }
}
