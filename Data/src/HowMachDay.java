import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/*
1.2.  Даны две даты в виде "25.07.1921", т.е. "день месяца 2 разряда, месяц 2 разряда, год 4 разряда".
Найти разницу в днях между ними. Число должно быть всегда положительным
*/

public class HowMachDay {
    public static void main(String[] args) {

        LocalDate day1 = LocalDate.of(2002, 12, 10);
        LocalDate day2 = LocalDate.of(2001, 12, 10);

        long days = ChronoUnit.DAYS.between(day1, day2);

        if (days < 0)
            days *= -1;

        System.out.println(days);
    }
}
