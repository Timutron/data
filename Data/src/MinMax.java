import java.util.Arrays;

/*
2.3. Даны три числа. Нужно найти минимум и максимум не используя условный и тернарный операторы
*/

public class MinMax {
    public static void main(String[] args) {
        int arr[] = {4, 2, 9};
        Arrays.sort(arr);
        int min = arr[0];
        int max = arr[2];

        System.out.println("Минимальное число: " + min);
        System.out.println("Максимальное число: " + max);
    }
}
