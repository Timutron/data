import java.math.BigDecimal;

/*
2.1.  Необходимо посчитать площадь круга с указанным радиусом с точностью 50 знаков после запятой
*/

public class SquareCircle {
    public static void main(String[] args) {
        double radius = 2;
        double square = Math.PI*(radius*radius);
        BigDecimal result = BigDecimal.valueOf(square).setScale(50);
        System.out.println(result);

    }
}
